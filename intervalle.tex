%%% Copyright (C) 2010-2022 Vincent Goulet, Mathieu Pigeon
%%%
%%% Ce fichier fait partie du projet «Analyse statistique - Exercices
%%% et solutions».
%%% https://gitlab.com/vigou3/analyse-statistique-exercices-et-solutions
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0 International de
%%% Creative Commons. https://creativecommons.org/licenses/by-sa/4.0/

\chapter{Estimation par intervalle}
\label{chap:intervalle}

\Opensolutionfile{reponses}[reponses-intervalle]
\Opensolutionfile{solutions}[solutions-intervalle]

\begin{Filesave}{reponses}
\bigskip
\section*{Réponses}

\end{Filesave}

\begin{Filesave}{solutions}
\section*{Chapitre \ref*{chap:intervalle}}
\addcontentsline{toc}{section}{Chapitre \protect\ref*{chap:intervalle}}

\end{Filesave}

%%%
%%% Début des exercices
%%%

\begin{exercice}
  \label{ex:intervalle:moyenne}
  La valeur observée de la moyenne empirique $\bar{X}$ d'un
  échantillon aléatoire de taille $20$ tiré d'une $N(\mu, 80)$ est
  $81,2$. Déterminer un estimateur par intervalle de niveau $95~\%$
  pour $\mu$.
  \begin{rep}
    $(77,28, \, 85,12)$
  \end{rep}
  \begin{sol}
    On cherche deux statistiques $L$ et $U$ tel que
    \begin{equation*}
      \prob{L \leq \mu \leq U} = 1 - \alpha.
    \end{equation*}
    On sait que si $X_1, \dots, X_n$ est un échantillon aléatoire tiré
    d'une distribution $N(\mu, \sigma^2)$, alors $\bar{X} \sim N(\mu,
    \sigma^2/n)$ ou, de manière équivalente, que
    \begin{displaymath}
      \frac{\bar{X} - \mu}{\sigma / \sqrt{n}} \sim N(0, 1).
    \end{displaymath}
    Par conséquent,
    \begin{equation*}
      \Prob{-z_{\alpha/2} \leq \frac{\bar{X} - \mu}{\alpha/\sqrt{n}} \leq
        z_{\alpha/2}} = 1 - \alpha,
    \end{equation*}
    d'où
    \begin{equation*}
      \Prob{\bar{X} - \frac{\sigma}{\sqrt{n}}\, z_{\alpha/2} \leq \mu
        \leq \bar{X} + \frac{\sigma}{\sqrt{n}}\, z_{\alpha/2}} = 1 - \alpha.
    \end{equation*}
    Les statistiques $L$ et $U$ sont dès lors connues: $L = \bar{X} -
    \sigma z_{\alpha/2}/\sqrt{n}$ et $U = \bar{X} + \sigma
    z_{\alpha/2}/\sqrt{n}$. Un estimateur par intervalle de $\mu$ est
    donc
    \begin{equation*}
      (\bar{X} - \sigma z_{\alpha/2}/\sqrt{n}, \,
      \bar{X} + \sigma z_{\alpha/2}/\sqrt{n}).
    \end{equation*}
    Avec $n = 20$, $\sigma^2 = 80$ et $\bar{x} = 81,2$, on obtient
    l'intervalle $(77,28, \, 85,12)$.
  \end{sol}
\end{exercice}

\begin{exercice}
  Soit $\bar{X}$ la moyenne d'un échantillon aléatoire de taille $n$
  d'une distribution normale de moyenne $\mu$ inconnue et de variance
  $9$. Trouver la valeur $n$ tel que, approximativement,
  $\prob{\bar{X} - 1 < \mu < \bar{X} + 1} = 0,90$.
  \begin{rep}
    $24$ ou $25$
  \end{rep}
  \begin{sol}
    On a $X \sim N(\mu, 9)$. Tel que démontré à
    l'\autoref{ex:intervalle:moyenne},
    \begin{equation*}
      \Prob{\bar{X} - \frac{\sigma}{\sqrt{n}}\, z_{0,05} \leq \mu
        \leq \bar{X} + \frac{\sigma}{\sqrt{n}}\, z_{0,05}} = 0,90.
    \end{equation*}
    Pour satisfaire la relation $\prob{\bar{X} - 1 < \mu < \bar{X} +
      1} = 0,90$, on doit donc choisir
    \begin{equation*}
      \frac{\sigma}{\sqrt{n}}\, z_{0,05} =
      \frac{3 (1,645)}{\sqrt{n}} = 1.
    \end{equation*}
    On trouve que $n = 24,35$. On doit donc choisir une taille
    d'échantillon de $24$ ou $25$.
  \end{sol}
\end{exercice}

\begin{exercice}
  Un échantillon aléatoire comptant $17$ observations d'une
  distribution normale de moyenne et de variance inconnues a donné
  $\bar{x} = 4,7$ et $s^2 = 5,76$. Trouver des intervalles de
  confiance à $90~\%$ pour $\mu$ et pour $\sigma^2$.
  \begin{rep}
    $\mu \in (3,7, \, 5,7)$ et $\sigma^2 \in (3,72, \, 12,30)$
  \end{rep}
  \begin{sol}
    On sait que
    \begin{align*}
      \frac{\bar{X} - \mu}{S/\sqrt{n - 1}} &\sim t(n - 1) \\
      \intertext{et que}
      \frac{n S^2}{\sigma^2} &\sim \chi^2(n - 1).
    \end{align*}
    Ainsi, on peut établir que
    \begin{equation*}
      \Prob{\bar{X} - \frac{S}{\sqrt{n - 1}}\, t_{\alpha/2} \leq \mu
        \leq \bar{X} + \frac{S}{\sqrt{n - 1}}\, t_{\alpha/2}} = 1 - \alpha.
    \end{equation*}
    et qu'un intervalle de confiance de niveau $1 - \alpha$ pour $\mu$
    est
    \begin{equation*}
      (\bar{X} - S t_{\alpha/2}/\sqrt{n - 1}, \,
      \bar{X} + S t_{\alpha/2}/\sqrt{n - 1}).
    \end{equation*}
    Avec $n = 17$, $\bar{x} = 4,7$, $s^2 = 5,76$ et $\alpha = 0,10$,
    on trouve que $\mu \in (3,7, \, 5,7)$.

    Pour la variance, on cherche des valeurs $a$ et $b$, $a \leq b$
    tel que
    \begin{equation*}
      \Prob{a \leq \frac{nS^2}{\sigma^2} \leq b} =
      \Prob{\frac{nS^2}{b} \leq \sigma^2 \leq \frac{nS^2}{a}} =
      1 - \alpha.
    \end{equation*}
    Plusieurs valeurs de $a$ et $b$ satisfont cette relation. Le choix
    le plus simple est $a = \chi_{1 - \alpha/2}^2(n - 1)$ et $b =
    \chi_{\alpha/2}^2(n - 1)$. Ainsi, un intervalle de confiance de
    niveau $1 - \alpha$ pour $\sigma^2$ est
    \begin{equation*}
      \left(
        \frac{nS^2}{\chi_{\alpha/2}^2(n - 1)}, \,
        \frac{nS^2}{\chi_{1 - \alpha/2}^2(n - 1)}
      \right).
    \end{equation*}
    Dans une table de la loi khi carré, on trouve que
    $\chi_{0,95}^2(16) = 7,96$ et que $\chi_{0,95}^2(16) = 26,30$,
    d'où $\sigma^2 \in (3,72, \, 12,30)$.
  \end{sol}
\end{exercice}

\begin{exercice}
  Lors d'une très sérieuse et importante analyse statistique de la
  taille des étudiantes en sciences et génie à l'Université Laval, on
  a mesuré un échantillon aléatoire d'étudiantes en actuariat et un
  autre en génie civil. Les résultats obtenus se trouvent résumés dans
  le tableau ci-dessous. On suppose que les deux échantillons
  aléatoires sont indépendants et que la taille des étudiantes est
  distribuée selon une loi normale.
  \begin{center}
    \begin{tabular}{lD{.}{,}{3.0}D{.}{,}{3.0}}
      \toprule
      Quantité &
      \multicolumn{1}{c}{Actuariat} &
      \multicolumn{1}{c}{Génie civil} \\
      \midrule
      Taille de l'échantillon     & 15 & 20 \\
      Taille moyenne (en cm)      & 152 & 154 \\
      Variance (en $\text{cm}^2$) & 101 & 112 \\
      \bottomrule
    \end{tabular}
  \end{center}
  \begin{enumerate}
  \item Déterminer un intervalle de confiance à $90~\%$ pour la taille
    moyenne des étudiantes de chacun des deux programmes en supposant
    que l'écart type de la distribution normale est 9~cm.
  \item Répéter la partie a) en utilisant plutôt les variances des
    échantillons.
  \item Y a-t-il une différence significative, avec un niveau de
    confiance de 90~\%, entre la taille des étudiantes en actuariat et
    celles en génie civil?
  \item Déterminer un intervalle de confiance à 90~\% pour la variance
    de la taille des étudiantes en actuariat.
  \item La différence observée entre les variances dans la taille des
    étudiantes des deux programmes est-elle significative? Utiliser un
    niveau de confiance de 90~\%.
  \end{enumerate}
  \begin{rep}
    \begin{enumerate}
    \item $148,18 < \mu_1 < 155,82$ et $150,69 < \mu_2 < 157,31$
    \item $147,27 < \mu_1 < 156,73$ et $149,80 < \mu_2 < 158,20$
    \item $\mu_1 - \mu_2 \in -2 \pm 5,99$
    \item $63,98 < \sigma_1^2 < 230,59$
    \item $0,417 < \sigma_2^2/\sigma_1^2 < 2,256$
    \end{enumerate}
  \end{rep}
  \begin{sol}
    On représente la taille des étudiantes en actuariat par la
    variable aléatoire $X$ et celle des étudiantes en génie civil par
    $Y$. On a
    \begin{align*}
      \bar{X} &\sim N(\mu_1, \sigma_1^2/15), &
      \bar{Y} &\sim N(\mu_2, \sigma_2^2/20), \\
      \frac{15 S_1^2}{\sigma_1^2} &\sim \chi^2(14), &
      \frac{20 S_2^2}{\sigma_2^2} &\sim \chi^2(19)
    \end{align*}
    et les valeurs des statistiques pour les deux échantillons sont
    \begin{align*}
      \bar{x} &= 152, & \bar{y} &= 154, \\
      s_1^2   &= 101, &  s_2^2  &= 112.
    \end{align*}
    \begin{enumerate}
    \item Si l'on suppose que $\sigma_1^2 = \sigma_2^2 = 81$, alors
      $\bar{X} \sim N(\mu_1, 5,4)$ et $\bar{Y} \sim N(\mu_2, 4,05)$.
      Par conséquent,
      \begin{gather*}
        \Prob{-1,645
          < \frac{\bar{X} - \mu_1}{\sqrt{5,4}} <
          1,645} = 0,90 \\
        \intertext{et}
        \Prob{-1,645
          < \frac{\bar{Y} - \mu_2}{\sqrt{4,05}} <
          1,645
        } = 0,90
      \end{gather*}
      d'où
      \begin{equation*}
        152 - 1,645 \sqrt{5,4} < \mu_1 < 152 + 1,645 \sqrt{5,4},
      \end{equation*}
      soit $148,18 < \mu_1 < 155,82$ et
      \begin{equation*}
        154 - 1,645 \sqrt{4,05} < \mu_2 < 154 + 1,645 \sqrt{4,05},
      \end{equation*}
      soit $150,69 < \mu_2 < 157,31$.
    \item Si la variance est inconnue, on a plutôt que
      \begin{equation*}
        \frac{\bar{X} - \mu_1}{S_1/\sqrt{14}} \sim t(14)
        \quad \text{et} \quad
        \frac{\bar{Y} - \mu_2}{S_2/\sqrt{19}} \sim t(19).
      \end{equation*}
      Or, $t_{0,05}(14) = 1,761$ et $t_{0,05}(19) =
      1,729$, d'où
      \begin{equation*}
        152 - 1,761 \sqrt{\frac{101}{14}} < \mu_1 < 152 + 1,761
        \sqrt{\frac{101}{14}},
      \end{equation*}
      soit $147,27 < \mu_1 < 156,73$ et
      \begin{equation*}
        154 - 1,729 \sqrt{\frac{112}{19}} < \mu_2 <
        154 + 1,729 \sqrt{\frac{112}{19}},
      \end{equation*}
      soit $149,80 < \mu_2 < 158,20$.
    \item On cherche un intervalle de confiance à $90~\%$ pour la
      différence $\mu_1 - \mu_2$. Si $0$ appartient à l'intervalle, on
      pourra dire que la différence entre les deux moyennes n'est pas
      significative à $90~\%$. Pour les besoins de la cause, on va
      supposer ici que $\sigma_1^2 = \sigma_2^2 = \sigma^2$. Or,
      puisque
      \begin{gather*}
        \frac{(\bar{X} - \bar{Y}) - (\mu_1 - \mu_2)}{%
          \sigma \sqrt{\frac{1}{15} + \frac{1}{20}}}
        \sim N(0, 1) \\
        \intertext{et que}
        \frac{15 S_1^2}{\sigma^2} + \frac{20 S_2^2}{\sigma^2}
        \sim \chi^2(33)
      \end{gather*}
      on établit que
      \begin{displaymath}
        \frac{(\bar{X} - \bar{Y}) - (\mu_1 - \mu_2)}{\sqrt{\frac{15
              S_1^2 + 20 S_2^2}{33} \left( \frac{1}{15} + \frac{1}{20}
            \right)}} \sim t(33).
      \end{displaymath}
      De plus, $t_{0,05}(33) \approx z_{0,05} = 1,645$, d'où
      l'intervalle de confiance à 90~\% pour $\mu_1 - \mu_2$ est
      \begin{displaymath}
        \mu_1 - \mu_2 \in -2 \pm 5,99.
      \end{displaymath}
      La différence de taille moyenne entre les deux groupes
      d'étudiantes n'est donc pas significative.
    \item Tel que mentionné précédemment,
      \begin{displaymath}
        Y = \frac{15 S_1^2}{\sigma_1^2} \sim \chi^2(14).
      \end{displaymath}
      Or, on trouve dans une table de la loi khi carré (ou avec la
      fonction \code{qchisq} dans R) que
      \begin{displaymath}
        \Prob{6,57 < Y < 23,68} = 0,90.
      \end{displaymath}
      Par conséquent,
      \begin{gather*}
        \Prob{
          6,57 < \frac{15 S_1^2}{\sigma_1^2} < 23,68
        } = 0,90 \\
        \intertext{ou, de manière équivalente,}
        \Prob{
          \frac{15 S_1^2}{23,68} < \sigma_1^2 < \frac{15 S_1^2}{6,57}
        } = 0,90.
      \end{gather*}
      Puisque $s_1^2 = 101$ dans cet exemple, un intervalle de
      confiance à $90~\%$ pour $\sigma_1^2$ est $(63,98, \, 230,59)$.
    \item Un peu comme en c), on détermine un intervalle de confiance
      pour le ratio $\sigma_2^2/\sigma_1^2$ et on conclut que la
      différence entre la variance des étudiantes en génie civil n'est
      pas significativement plus grande que celle des étudiantes en
      actuariat si cet intervalle contient la valeur $1$. À la suite
      des conclusions en c), il est raisonnable de supposer que les
      moyennes des deux populations sont identiques, soit $\mu_1 =
      \mu_2 = \mu$. On a que
      \begin{displaymath}
        F = \frac{15 S_1^2/(14 \sigma_1^2)}{20 S_2^2/(19 \sigma_2^2)} =
        \frac{57 S_1^2}{56 S_2^2} \frac{\sigma_2^2}{\sigma_1^2}
        \sim F(14, 19).
      \end{displaymath}
      On trouve dans une table de la loi $F$ (ou avec la fonction
      \code{qf} dans R) que
      \begin{displaymath}
        \Prob{0,417 < F < 2,256} = 0,90.
      \end{displaymath}
      Par conséquent,
      \begin{displaymath}
        \Prob{
          \frac{23,33 S_2^2}{57 S_1^2}
          < \frac{\sigma_2^2}{\sigma_1^2} <
          \frac{126,31 S_2^2}{57 S_1^2}
        } = 0,90
      \end{displaymath}
      et un intervalle de confiance à $90~\%$ pour
      $\sigma_2^2/\sigma_1^2$ est $(0,417, \, 2,256)$. La variance
      $\sigma_2^2$ n'est donc pas significativement plus grande que
      $\sigma_1^2$.
    \end{enumerate}
  \end{sol}
\end{exercice}

\begin{exercice}
  Soit $X_1, \dots, X_n$ un échantillon aléatoire tiré d'une
  population normale de moyenne et variance inconnues. Développer la
  formule d'un estimateur par intervalle de niveau $1 - \alpha$ pour
  $\sigma$, l'écart type de la distribution normale.
  \begin{rep}
    $(\sqrt{n S^2/b}, \sqrt{n S^2/a})$.
  \end{rep}
  \begin{sol}
    On sait que
    \begin{displaymath}
      \frac{nS^2}{\sigma^2} \sim \chi^2(n-1).
    \end{displaymath}
    Ainsi, pour des constantes $a$ et $b$, $a \leq b$, on a
    \begin{equation*}
      \Prob{a \leq \frac{nS^2}{\sigma^2} \leq b} =
      \Prob{\sqrt{\frac{nS^2}{b}} \leq \sigma \leq \sqrt{\frac{nS^2}{a}}} =
      1 - \alpha.
    \end{equation*}
    Un estimateur par intervalle de $\sigma$ est donc $(\sqrt{n
      S^2/b}, \sqrt{n S^2/a})$, où $a$ et $b$ satisfont la relation
    $\prob{a \leq Y \leq b} = 1 - \alpha$, avec $Y \sim \chi^2(n - 1)$.
  \end{sol}
\end{exercice}

\begin{exercice}
  Soit $X_1, X_2, \dots, X_n$ un échantillon aléatoire d'une
  distribution normale de moyenne $\mu$ et de variance $\sigma^2 =
  25$. Déterminer la taille de l'échantillon nécessaire pour que la
  longueur de l'intervalle de confiance de niveau $0,90$ pour la
  moyenne ne dépasse pas $0,05$.
  \begin{rep}
    \nombre{108 241}
  \end{rep}
  \begin{sol}
    On sait que
    \begin{align*}
      \mu &\in \bar{X} \pm z_{0,05} \frac{\sigma}{\sqrt{n}} \\
      &\in \bar{X} \pm 1,645 \frac{5}{\sqrt{n}}.
    \end{align*}
    La longueur de l'intervalle de confiance est $2 (1,645) (5) /
    \sqrt{n} = 16,45/\sqrt{n}$. Si l'on souhaite que $16,45/\sqrt{n}
    \leq 0,05$, alors $n \geq \nombre{108 241}$.
  \end{sol}
\end{exercice}

\begin{exercice}
  Soit $S^2$ la variance d'un échantillon aléatoire de taille $n$ issu
  d'une distribution $N(\mu, \sigma^2)$ où $\mu$ et $\sigma^2$ sont
  des paramètres inconnus. On sait que $Y = nS^2/\sigma^2 \sim
  \chi^2(n - 1)$. Soit $g(y)$ la fonction de densité de $Y$ et $G(y)$
  la fonction de répartition. Soit $a$ et $b$ des constantes telles
  que $(n s^2/b, n s^2/a)$ est un intervalle de confiance de niveau $1
  - \alpha$ pour $\sigma^2$. La longueur de cet intervalle est donc $n
  s^2 (b - a)/(ab)$. Démontrer que la longueur de l'intervalle de
  confiance est minimale si $a$ et $b$ satisfont la condition $a^2
  g(a) = b^2 g(b)$. (\emph{Astuce}: minimiser la longueur de
  l'intervalle sous la contrainte que $G(b) - G(a) = 1 - \alpha$.)
  \begin{sol}
    On cherche à minimiser la longueur de l'intervalle de confiance
    $h(a, b) = n s^2 (b - a)/(ab)$ sous la contrainte que la
    probabilité dans cet intervalle est $1 - \alpha$, c'est-à-dire que
    $G(b) - G(a) = 1 - \alpha$. En utilisant la méthode des
    multiplicateurs de Lagrange, on pose
    \begin{displaymath}
      L(a, b, \lambda) = \frac{n s^2}{a} - \frac{n b^2}{b} +
      \lambda(G(b) - G(a) - 1 + \alpha).
    \end{displaymath}
    Les dérivées de cette fonction par rapport à chacune de ses
    variables sont:
    \begin{align*}
      \frac{\partial}{\partial a}\, L(a, b, \lambda)
      &= - \frac{n s^2}{a^2} + \lambda g(a) \\
      \frac{\partial}{\partial b}\, L(a, b, \lambda)
      &= - \frac{-n s^2}{b^2} + \lambda g(b) \\
      \frac{\partial}{\partial \lambda}\, L(a, b, \lambda)
      &= G(b) - G(a) - 1 + \alpha.
    \end{align*}
    En posant ces dérivées égales à zéro et en résolvant, on trouve
    que
    \begin{equation*}
      \frac{g(a)}{g(b)} = \frac{b^2}{a^2}
    \end{equation*}
    ou, de manière équivalente, que $b^2 g(b) = a^2 g(a)$.
  \end{sol}
\end{exercice}

\Closesolutionfile{solutions}
\Closesolutionfile{reponses}

\section*{Exercices proposés dans \cite{Wackerly:mathstat:7e:2008}}

\begin{trivlist}
\item 8.39, 8.41, 8.43, 8.47, 8.57, 8.59, 8.63, 8.70, 8.72, 8.75, 8.77
\end{trivlist}


%%%
%%% Insérer les réponses
%%%
%\input{reponses-intervalle}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "analyse-statistique-exercices-et-solutions"
%%% coding: utf-8
%%% End:
