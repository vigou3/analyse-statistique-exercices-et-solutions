%%% Copyright (C) 2010-2022 Vincent Goulet, Mathieu Pigeon
%%%
%%% Ce fichier et tous les fichiers .tex ou .Rnw dont la racine est
%%% mentionnée dans les commandes \include ci-dessous font partie du
%%% projet «Analyse statistique - Exercices et solutions».
%%% https://gitlab.com/vigou3/analyse-statistique-exercices-et-solutions
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[letterpaper,11pt,x11names,english,french]{memoir}
  \usepackage{natbib,url}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{vgmath,vgsets,amsmath,amsthm}
  \usepackage[noae]{Sweave}
  \usepackage{graphicx}
  \usepackage[absolute]{textpos}            % couvertures
  \usepackage[shortlabels,inline]{enumitem} % configuration listes
  \usepackage{longtable,lscape}             % table de loi F
  \usepackage{relsize}                      % \smaller et al.
  \usepackage{manfnt}                       % \mantriangleright (puce)
  \usepackage{metalogo}                     % logo \XeLaTeX
  \usepackage{fontawesome5}                 % icônes \fa*
  \usepackage{answers}

  %%% =============================
  %%%  Informations de publication
  %%% =============================
  \title{Analyse statistique --- Exercices et solutions}
  \author{Vincent Goulet \and Mathieu Pigeon}
  \renewcommand{\year}{2022}
  \renewcommand{\month}{12}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/analyse-statistique-exercices-et-solutions}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Polices de caractères
  \usepackage{fontspec}
  \usepackage{unicode-math}
  \defaultfontfeatures
  {
    Scale = 0.92
  }
  \setmainfont{Lucida Bright OT}
  [
    Ligatures = TeX,
    Numbers = OldStyle
  ]
  \setmathfont{Lucida Bright Math OT}
  \setmonofont{Lucida Grande Mono DK}
  \setsansfont{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    ItalicFont = *-BookItalic,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 0.98,
    Numbers = OldStyle
  ]
  \newfontfamily\fullcaps{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    Scale = 0.98,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}
  \usepackage{icomma}

  %% Couleurs
  \usepackage{xcolor}
  \definecolor{comments}{rgb}{0.5,0.55,0.6} % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}        % liens internes
  \definecolor{url}{rgb}{0.6,0,0}           % liens externes
  \definecolor{citation}{rgb}{0,0.5,0}      % citations
  \colorlet{codebg}{LightYellow1}           % fond code R
  \definecolor{rouge}{rgb}{0.90,0,0.1}      % rouge bandeau UL
  \definecolor{or}{rgb}{1,0.8,0}            % or bandeau UL

  %% Hyperliens
  \usepackage{hyperref}
  \hypersetup{%
    pdfauthor = \theauthor,
    pdftitle = \thetitle,
    colorlinks = true,
    linktocpage = true,
    urlcolor = {url},
    linkcolor = {link},
    citecolor = {citation},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit}}
  \setlength{\XeTeXLinkMargin}{1pt}

  %% Affichage de la table des matières du PDF
  \usepackage{bookmark}
  \bookmarksetup{%
    open = true,
    depth = 3,
    numbered = true}

  %% Étiquettes de \autoref (redéfinitions compatibles avec babel).
  %% Attention! Les % à la fin des lignes sont importants sinon des
  %% blancs apparaissent dès que la commande \selectlanguage est
  %% utilisée... comme dans la bibliographie, par exemple.
  \addto\extrasfrench{%
    \def\appendixautorefname{annexe}%
    \def\figureautorefname{figure}%
    \def\exerciceautorefname{exercice}%
    \def\subfigureautorefname{figure}%
  }

  %% Table des matières et al. (inspiré de classicthesis.sty)
  \renewcommand{\cftchapterleader}{\hspace{1.5em}}
  \renewcommand{\cftchapterafterpnum}{\cftparfillskip}
  \renewcommand{\cftsectionleader}{\hspace{1.5em}}
  \renewcommand{\cftsectionafterpnum}{\cftparfillskip}
  \renewcommand{\cfttableleader}{\hspace{1.5em}}
  \renewcommand{\cfttableafterpnum}{\cftparfillskip}
  \renewcommand{\cftfigureleader}{\hspace{1.5em}}
  \renewcommand{\cftfigureafterpnum}{\cftparfillskip}
  \setlength{\cftbeforechapterskip}{1.0em plus 0.5em minus 0.5em}
  \setlength{\cftfigurenumwidth}{3.2em}

  %% Titres des chapitres
  \chapterstyle{hangnum}
  \renewcommand{\chaptitlefont}{\normalfont\Huge\sffamily\bfseries\raggedright}

  %% Marges, entêtes et pieds de page
  \setlength{\marginparsep}{7mm}
  \setlength{\marginparwidth}{13mm}
  \setlength{\headwidth}{\textwidth}
  \addtolength{\headwidth}{\marginparsep}
  \addtolength{\headwidth}{\marginparwidth}

  %% Listes. Paramétrage avec enumitem.
  \setlist[enumerate]{leftmargin=*,align=left}
  \setlist[enumerate,2]{label=\alph*),labelsep=*,leftmargin=1.5em}
  \setlist[enumerate,3]{label=\roman*),labelsep=*,leftmargin=1.5em,align=right}
  \setlist[itemize]{leftmargin=*,align=left}
  \setlist[trivlist]{nosep}

  %% Options de babel
  \frenchbsetup{CompactItemize=false,%
    ThinSpaceInFrenchNumbers=true,
    ItemLabeli=\mantriangleright,
    ItemLabelii=\textendash,
    og=«, fg=»}
  \def\frenchtablename{{\scshape Tab.}}
  \def\frenchfigurename{{\scshape Fig.}}
  \def\frenchlistfigurename{Liste des figures}

  %%% =========================
  %%%  Nouveaux environnements
  %%% =========================

  %% Environnements de Sweave. Les environnements Sinput et Soutput
  %% utilisent Verbatim (de fancyvrb). On les réinitialise pour
  %% enlever la configuration par défaut de Sweave, puis on réduit
  %% l'écart entre les blocs Sinput et Soutput.
  \DefineVerbatimEnvironment{Sinput}{Verbatim}{}
  \DefineVerbatimEnvironment{Soutput}{Verbatim}{}
  \fvset{listparameters={\setlength{\topsep}{0pt}}}

  %% L'environnement Schunk est complètement redéfini en un hybride
  %% des environnements snugshade* et leftbar de framed.sty.
  \makeatletter
  \renewenvironment{Schunk}{%
    \setlength{\topsep}{1pt}
    \def\FrameCommand##1{\hskip\@totalleftmargin
       \vrule width 2pt\colorbox{codebg}{\hspace{3pt}##1}%
      % There is no \@totalrightmargin, so:
      \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
    \MakeFramed {\advance\hsize-\width
      \@totalleftmargin\z@ \linewidth\hsize
      \advance\labelsep\fboxsep
      \@setminipage}%
  }{\par\unskip\@minipagefalse\endMakeFramed}
  \makeatother

  %% Exercices et réponses
  \Newassociation{sol}{solution}{solutions}
  \Newassociation{rep}{reponse}{reponses}
  \newcounter{exercice}[chapter]
  \renewcommand{\theexercice}{\thechapter.\arabic{exercice}}
  \newenvironment{exercice}[1][]{%
    \begin{list}{}{%
        \refstepcounter{exercice}
        \ifthenelse{\equal{#1}{nosol}}{%
          \renewcommand{\makelabel}{\bfseries\theexercice}}{%
          \hypertarget{ex:\theexercice}{}
          \Writetofile{solutions}{\protect\hypertarget{sol:\theexercice}{}}
          \renewcommand{\makelabel}{%
            \bfseries\protect\hyperlink{sol:\theexercice}{\theexercice}}}
        \settowidth{\labelwidth}{\bfseries\theexercice}
        \setlength{\leftmargin}{\labelwidth}
        \addtolength{\leftmargin}{\labelsep}
        \setlist[enumerate,1]{label=\alph*),labelsep=*,leftmargin=1.5em}
        \setlist[enumerate,2]{label=\roman*),labelsep=0.5em,align=right}}
    \item}%
    {\end{list}}
  \renewenvironment{reponse}[1]{%
    \begin{list}{\bfseries #1}{%
        \settowidth{\labelwidth}{#1}
        \setlength{\leftmargin}{\labelwidth}
        \addtolength{\leftmargin}{\labelsep}
        \setlist[enumerate,1]{label=\alph*),labelsep=*,leftmargin=1.5em}
        \setlist[enumerate,2]{label=\roman*),labelsep=0.5em,align=right}}
    \item}
    {\end{list}}
  \renewcommand{\reponseparams}{{\thechapter.\theexercice}}
  \renewenvironment{solution}[1]{%
    \begin{list}{}{%
        \renewcommand{\makelabel}{%
          \bfseries\protect\hyperlink{ex:#1}{#1}}
        \settowidth{\labelwidth}{\bfseries #1}
        \setlength{\leftmargin}{\labelwidth}
        \addtolength{\leftmargin}{\labelsep}
        \setlist[enumerate,1]{label=\alph*),labelsep=*,leftmargin=1.5em}
        \setlist[enumerate,2]{label=\roman*),labelsep=0.5em,align=right}}
    \item}
    {\end{list}}

  %% Corriger un bogue dans answers apparu dans TeX Live 2020.
  %% https://tex.stackexchange.com/a/537873
  \makeatletter
  \renewcommand{\Writetofile}[2]{%
    \@bsphack
    \Iffileundefined{#1}{}{%
      \Ifopen{#1}{%
        {%
          \begingroup
          % \let\protect\string %%% <----- removed
          \Ifanswerfiles{%
            \protected@iwrite{\@nameuse{#1@file}}{\afterassignment\let@protect@string}{#2}%
          }{}%
          \endgroup
        }%
      }{}%
    }%
    \@esphack
  }
  \def\let@protect@string{\let\protect\string}
  \makeatother

  %%% =====================
  %%%  Nouvelles commandes
  %%% =====================

  %% Noms de fonctions, code, etc.
  \newcommand{\code}[1]{\texttt{#1}}

  %% Opérateurs mathématiques additionnels
  \newcommand{\MSE}{\operatorname{MSE}}

  %% Lien vers GitLab dans la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Hyperlien avec symbole de lien externe juste après; second
  %% argument peut être vide pour afficher l'url comme lien
  %% [https://tex.stackexchange.com/q/53068/24355 pour procédure de
  %% test du second paramètre vide]
  \newcommand{\link}[2]{%
    \def\param{#2}%
    \ifx\param\empty
      \href{#1}{\nolinkurl{#1}~{\smaller\faExternalLink*}}%
    \else
      \href{#1}{#2~{\smaller\faExternalLink*}}%
    \fi
  }

  %%% =======
  %%%  Varia
  %%% =======

  %%% Sous-figures
  \newsubfloat{figure}

  %%% Style de la bibliographie
  \bibliographystyle{francais}

  %% Longueurs pour la composition des pages couvertures avant et
  %% arrière.
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}
  \newlength{\logoheight}

  %%% Aide pour la césure
  \hyphenation{con-fiance}
  \hyphenation{con-train-te}

%  \includeonly{}

\begin{document}

\frontmatter

\pagestyle{empty}

\input{couverture-avant}
\null\cleartoverso              % cf. section 2.2 textpos.pdf

\include{notices}
\clearpage

\pagestyle{companion}

\include{introduction}

\tableofcontents
\cleartorecto
\listoffigures

\mainmatter

\include{rappels}
\include{transformations}
\include{echantillon}
\include{ponctuelle}
\include{bayesienne}
\include{intervalle}
\include{tests}

\appendix
\include{solutions}

\nocite{Hogg:mathstat:6e:2005,
  Hogg:probstat:2001,
  Mood:mathstat:1974,
  Freund:mathstat:1992}
\bibliography{stat,r}

\cleartoverso
\pagestyle{empty}
\input{colophon}

\cleartoverso
\input{couverture-arriere}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: t
%%% coding: utf-8
%%% End:
