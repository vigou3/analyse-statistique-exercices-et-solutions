%%% Copyright (C) 2010-2022 Vincent Goulet, Mathieu Pigeon
%%%
%%% Ce fichier fait partie du projet «Analyse statistique - Exercices
%%% et solutions».
%%% https://gitlab.com/vigou3/analyse-statistique-exercices-et-solutions
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0 International de
%%% Creative Commons. https://creativecommons.org/licenses/by-sa/4.0/

\chapter{Estimation bayésienne}
\label{chap:bayesienne}

\Opensolutionfile{reponses}[reponses-bayesienne]
\Opensolutionfile{solutions}[solutions-bayesienne]

\begin{Filesave}{reponses}
\bigskip
\section*{Réponses}

\end{Filesave}

\begin{Filesave}{solutions}
\section*{Chapitre \ref*{chap:bayesienne}}
\addcontentsline{toc}{section}{Chapitre \protect\ref*{chap:bayesienne}}

\end{Filesave}

%%%
%%% Début des exercices
%%%

\begin{exercice}
  On tire une observation d'une loi de Poisson de moyenne $\lambda$
  égale à 2 ou 4. On croit que la valeur $\lambda = 2$ est quatre fois
  plus probable que $\lambda = 4$.
  \begin{enumerate}
  \item Calculer une estimation du paramètre $\lambda$ minimisant
    l'erreur quadratique moyenne avant que l'observation ne soit
    disponible.
  \item L'expérience effectuée, la valeur de l'observation est $x =
    6$. Quelle est la nouvelle distribution de probabilité du
    paramètre $\lambda$.
  \item Répéter la partie a) maintenant que l'observation mentionnée
    en b) est disponible.
  \end{enumerate}
  \begin{rep}
    \begin{enumerate*}
    \item $12/5$
    \item $\prob{\Lambda = 2|X = 6} = 0,316$, $\prob{\Lambda = 4|X =
        6} = 0,684$
    \item $3,368$
    \end{enumerate*}
  \end{rep}
  \begin{sol}
    On rend compte de l'incertitude sur la valeur du paramètre
    de la loi de Poisson en considérant ce paramètre comme une
    variable aléatoire. On obtient alors le modèle bayésien suivant:
    \begin{align*}
      X &\sim \text{Poisson}(\Lambda) \\
      \prob{\Lambda = \lambda} &=
      \begin{cases}
        4/5, & \lambda = 2 \\
        1/5, & \lambda = 4.
      \end{cases}
    \end{align*}
    \begin{enumerate}
    \item On cherche la valeur $\hat{\lambda}$ qui minimise
      $\esp{(\Lambda - \hat{\lambda})^2}$. De
      l'\autoref{ex:rappels:moyenne}, on sait que
      $\hat{\lambda} = \esp{\Lambda} = 2 (4/5) + 4(1/5) = 12/5 = 2,4$.
    \item On veut calculer $\prob{\Lambda = \lambda|X = 6}$. Par la
      règle de Bayes, on a que
      \begin{equation*}
        \prob{\Lambda = \lambda|X = 6} =
        \frac{\prob{X = 6|\Lambda = \lambda} \prob{\Lambda =
            \lambda}}{%
          \sum_{\lambda = 1, 2} \prob{X = 6|\Lambda = \lambda} \prob{\Lambda =
            \lambda}}, \quad \lambda = 1, 2.
      \end{equation*}
      Puisque
      \begin{align*}
        \prob{X = 6|\Lambda = 2} &= \frac{2^6 e^{-2}}{6!} \\
        \prob{X = 6|\Lambda = 4} &= \frac{4^6 e^{-4}}{6!},
      \end{align*}
      on obtient
      \begin{equation*}
        \prob{\Lambda = \lambda|X = 6} =
        \begin{cases}
          0,316, & \lambda = 2 \\
          0,684, & \lambda = 4.
        \end{cases}
      \end{equation*}
    \item Par analogie avec la partie a), la valeur qui minimise
      $\esp{(\Lambda - \hat{\lambda})^2|X = 6}$ est $\hat{\lambda} =
      \esp{\Lambda|X = 6} = 2 (0,316) + 4 (0,684) = 3,368$.
    \end{enumerate}
  \end{sol}
\end{exercice}

\begin{exercice}
  Trouver l'estimateur bayésien de la probabilité de succès d'une
  loi géométrique si la distribution a priori du paramètre est une loi
  $\text{Bêta}(\alpha, \beta)$. Utiliser la loi géométrique avec
  support $x = 0, 1, 2, \dots$ et une fonction de perte quadratique.
  \begin{rep}
    $(\alpha + n)/(\alpha + \beta + n + \sum_{i = 1}^n X_i)$
  \end{rep}
  \begin{sol}
    On pose $X|\Theta = \theta \sim \text{Géométrique}(\theta)$ et
    $\Theta \sim \text{Bêta}(\alpha, \beta)$. Ainsi,
    \begin{align*}
      f(x|\theta)
      &= \theta (1 - \theta)^x, \quad x = 0, 1, \dots \\
      u(\theta)
      &= \frac{\Gamma(\alpha, \beta)}{\Gamma(\alpha) \Gamma(\beta)}\,
      \theta^{\alpha - 1}(1 - \theta)^{\beta - 1}, \quad 0 < \theta < 1
      \intertext{et, par conséquent,}
      f(x_1, \dots, x_n|\theta)
      &= \prod_{i = 1}^n f(x_i|\theta) = \theta^n (1 - \theta)^{\sum_{i=1}^n x_i}.
    \end{align*}
    Soit $u(\theta|x_1, \dots, x_n)$ la densité a posteriori de la variable
    aléatoire $\Theta$. Par le théorème de Bayes,
    \begin{align*}
      u(\theta|x_1, \dots, x_n)
      &= \frac{f(x_1, \dots, x_n|\theta) u(\theta)}{%
        f(x_1, \dots, x_n)} \\
      &= \frac{f(x_1, \dots, x_n|\theta) u(\theta)}{%
        \int_0^1 f(x_1, \dots, x_n|\theta) u(\theta)\, d\theta} \\
      &\propto f(x_1, \dots, x_n|\theta) u(\theta) \\
      &= \theta^n (1 - \theta)^{\sum_{i = 1}^n x_i}
      \theta^{\alpha - 1}(1 - \theta)^{\beta - 1} \\
      &= \theta^{n + \alpha - 1} (1 - \theta)^{\sum_{i = 1}^n x_i +
        \beta - 1}.
    \end{align*}
    On reconnaît dans cette dernière expression la densité, à une
    constante près, d'une distribution Bêta$(\alpha + n, \sum_{i=1}^n
    + \beta)$.

    Enfin, on sait qu'en utilisant une fonction de perte quadratique,
    l'estimateur bayésien ponctuel de $\theta$ est:
    \begin{align*}
      \hat{\theta} &= \esp{\Theta|X_1, \dots, X_n} \\
      &= \int_0^1 \theta u(\theta|X_1, \dots, X_n)\, d\theta \\
      &= \frac{\alpha + n}{\alpha + \beta + n +
        \sum_{i=1}^n X_i}.
    \end{align*}
    La dernière égalité n'est en fait que l'espérance d'une
    distribution bêta, un résultat connu.
  \end{sol}
\end{exercice}

\begin{exercice}
  Soit $X_1, \dots, X_n$ un échantillon aléatoire d'une distribution
  de Poisson avec moyenne inconnue $\theta$. Si l'on utilise une
  fonction de perte quadratique et que l'on suppose que $\theta$ est
  une observation d'une loi gamma de paramètres $\alpha$ et $\lambda$,
  quel est l'estimateur bayésien de $\theta$?
  \begin{rep}
    $(\alpha + \sum_{i=1}^n X_i)/(\lambda + n)$
  \end{rep}
  \begin{sol}
    On a $X|\Theta = \theta \sim \text{Poisson}(\theta)$ et $\Theta
    \sim \text{Gamma}(\alpha, \lambda)$, soit
    \begin{align*}
      f(x|\theta)
      &= \frac{\theta^x e^{-\theta}}{x!}, \quad x = 0, 1, \dots \\
      u(\theta)
      &= \frac{\lambda^\alpha}{\Gamma(\alpha)}\,
      \theta^{\alpha - 1} e^{-\lambda \theta}, \quad \theta > 0 \\
      \intertext{et, par conséquent,}
      f(x_1, \dots, x_n|\theta)
      &=
    \end{align*}
    Par le théorème de Bayes, on obtient la densité de la variable
    aléatoire $\Theta|X_1 = x_1, \dots, X_n = x_n$:
    \begin{align*}
      u(\theta|x_1, \dots, x_n)
      &= \frac{f(x_1, \dots, x_n|\theta) u(\theta)}{%
        \int_0^\infty f(x_1, \dots, x_n|\theta) u(\theta)\, d\theta} \\
      &\propto
      \left(
        \prod_{i = 1}^n f(x_i|\theta)
      \right) u(\theta) \\
      &\propto (\theta^{\sum_{i=1}^n x_i} e^{-n\theta})
      (\theta^{\alpha - 1} e^{-\lambda \theta}) \\
      &= \theta^{\sum_{i=1}^n x_i + \alpha - 1} e^{-(n + \lambda) \theta}.
    \end{align*}
    On reconnaît, à une constante près, la densité d'une distribution
    gamma. Ainsi, $\Theta|X_1, \dots, X_n \sim
    \text{Gamma}(\sum_{i=1}^n x_i + \alpha, \lambda + n)$. En
    utilisant une fonction de perte quadratique, l'estimateur bayésien
    du paramètre $\theta$ est:
    \begin{align*}
      \hat{\theta} &= \esp{\Theta|X_1, \dots, X_n} \\
      &= \frac{\alpha + \sum_{i=1}^n X_i}{n + \lambda}.
    \end{align*}
  \end{sol}
\end{exercice}

\begin{exercice}
  L'estimateur bayésien du paramètre d'une loi de Bernoulli est
  \begin{equation*}
    \hat{\theta} = \frac{\alpha + \sum_{i=1}^n X_i}{\alpha + \beta + n}
  \end{equation*}
  lorsque la distribution a priori du paramètre est une
  $\text{Bêta}(\alpha, \beta)$ et que la fonction de perte utilisée
  est $L(\theta, \hat{\theta}) = (\theta - \hat{\theta})^2$.
  \begin{enumerate}
  \item Soit $n = 30$, $\alpha = 15$ et $\beta = 5$. En notant que
    $\sum_{i=1}^{30} X_i \sim \text{Binomiale}(30, \theta)$, calculer
    l'espérance de la fonction de perte en fonction de $\theta$.
    (\emph{Astuce}: écrire l'erreur quadratique moyenne comme la somme
    de la variance et du biais au carré.)
  \item L'erreur quadratique moyenne de $\bar{X}$ --- l'estimateur du
    maximum de vraisemblance de $\theta$ --- est $\theta (1 -
    \theta)/30$, cet estimateur étant sans biais. Trouver pour quelles
    valeurs de $\theta$ l'erreur de l'estimateur bayésien en a) est
    inférieure à celle de l'estimateur du maximum de vraisemblance.
  \end{enumerate}
  \begin{rep}
    \begin{enumerate*}
    \item $(74 \theta^2 - 114 \theta + 45)/500$
    \item $0,5692 < \theta <  0,8720$
    \end{enumerate*}
  \end{rep}
  \begin{sol}
    \begin{enumerate}
    \item On écrit l'estimateur sous la forme
      \begin{equation*}
        \hat{\theta} = \frac{\alpha + Y}{\alpha + \beta + n}
      \end{equation*}
      avec $Y \sim \text{Binomiale}(n, \theta)$. On remarquera que
      $\hat{\theta}$ est une variable aléatoire. Ainsi,
      $\esp{(\hat{\theta} - \theta)^2} = \var{\hat{\theta}} +
      (\esp{\hat{\theta}} - \theta)^2$. Or,
      \begin{align*}
        \var{\hat{\theta}}
        &= \frac{\var{Y}}{(\alpha + \beta + n)^2} \\
        &= \frac{n \theta (1 - \theta)}{(\alpha + \beta + n)^2} \\
        \intertext{et}
        \esp{\hat{\theta}}
        &= \frac{\alpha + \esp{Y}}{\alpha + \beta + n} \\
        &= \frac{\alpha + n \theta}{\alpha + \beta + n},
      \end{align*}
      d'où
      \begin{align*}
        \esp{(\hat{\theta} - \theta)^2}
        &= \frac{n \theta (1 - \theta)}{(\alpha + \beta + n)^2} +
        \left(
          \frac{(1 - \theta) \alpha - \beta \theta}{\alpha + \beta + n}
        \right)^2.
      \end{align*}
      En posant $n = 30$, $\alpha = 15$ et $\beta = 5$, on trouve
      \begin{align*}
        \esp{(\hat{\theta} - \theta)^2}
        &= \frac{370\theta^2 - 570\theta + 225}{\nombre{2500}} \\
        &= \frac{74\theta^2 - 114\theta + 45}{500}.
      \end{align*}
    \item On cherche pour quelles valeurs de $\theta$ l'inégalité
      \begin{displaymath}
        \frac{74\theta^2 -114\theta+45}{500} <
        \frac{\theta - \theta^2}{30}
      \end{displaymath}
      est satisfaite. Cette inégalité peut se réécrire sous la forme
      \begin{equation*}
        272 \theta^2 - 392 \theta + 135 > 0.
      \end{equation*}
      Elle est satisfaite lorsque $\theta$ se trouve entre les deux
      racines du polynôme du côté droit de l'inégalité. On trouve donc
      aisément que l'erreur quadratique moyenne de l'estimateur
      bayésien est inférieure à celle de l'estimateur du maximum de
      vraisemblance lorsque $0,5692 < \theta < 0,8720$.
    \end{enumerate}
  \end{sol}
\end{exercice}

\begin{exercice}
  Soit $X_1, \dots, X_n$ un échantillon aléatoire d'une distribution
  normale de moyenne $\theta$ et de variance $\sigma^2$, où $\sigma^2$
  est une constante connue. On suppose que $\theta$ est une
  observation d'une variable aléatoire $\Theta$ avec distribution
  Normale$(\mu, \tau^2)$, où $\mu$ et $\tau^2$ sont des constantes
  connues.
  \begin{enumerate}
  \item Démontrer que la distribution a posteriori de la variable
    aléatoire $\Theta$ est $N(\tilde{\mu}, \tilde{\tau}^2)$,
    avec
    \begin{align*}
      \tilde{\mu}
      &= \frac{\tau^2 \sum_{i=1}^n X_i + \sigma^2 \mu}{%
        n \tau^2 + \sigma^2} \\
      \tilde{\tau}^2
      &= \frac{\tau^2 \sigma^2}{n \tau^2 + \sigma^2}.
    \end{align*}
    (\emph{Astuce}: calculer la distribution a posteriori à une
    constante de normalisation près. Pour ce faire, compléter le carré
    à l'exposant en ne conservant que les termes impliquant $\theta$.)
  \item Trouver un estimateur ponctuel bayésien de $\theta$ si la
    fonction de perte utilisée est $L(\theta, \hat{\theta}) = |\theta
    - \hat{\theta}|$.
  \end{enumerate}
  \begin{rep}
    \begin{enumerate}
      \stepcounter{enumi}
    \item $\tilde{\mu}$.
    \end{enumerate}
  \end{rep}
  \begin{sol}
    \begin{enumerate}
    \item On a %
      $X|\Theta = \theta \sim N(\theta,\sigma^2)$ %
      et $\Theta \sim N(\mu, \tau^2)$, soit
      \begin{align*}
        f(x|\theta) &=
        \left(
          \frac{1}{\sigma \sqrt{2\pi}}
        \right)^n
        e^{-(x - \theta)^2/(2 \sigma^2)}, \quad -\infty < x < \infty \\
        u(\theta)
        &= \frac{1}{\tau \sqrt{2\pi}}\,
        e^{(\theta - \mu)/(2 \tau^2)}, \quad -\infty < \theta < \infty.
      \end{align*}
      La distribution a posteriori de $\Theta$ est, en laissant de côté
      tous les termes qui ne dépendent pas de $\theta$:
      \begin{equation*}
        u(\theta|x_1, \dots, x_n) \propto
        e^{-(\theta - \mu)^2/(2 \tau^2)}\,
        e^{- \sum_{t=1}^n (x_t - \theta)^2/(2 \sigma^2)}.
      \end{equation*}
      En développant l'exposant tout en laissant une fois de plus de
      côté tous les termes non fonction de $\theta$, on obtient
      \begin{align*}
        \text{Exposant}
        &= - \frac{1}{2}
        \left[
          \frac{\theta^2 - 2\theta \mu}{\tau^2} +
          \sum_{t=1}^n \frac{-2 \theta x_t + \theta^2}{\sigma^2} +
          \text{cte}
        \right] \\
        &= - \frac{1}{2}
        \left[
          \theta^2
          \left(
            \frac{1}{\tau^2} + \frac{n}{\sigma^2}
          \right) -
          \frac{2 \theta \mu}{\tau^2} -
          \frac{-2 \theta \sum_{t=1}^n x_t}{\sigma^2} +
          \text{cte}
        \right] \displaybreak[0] \\
        &= - \frac{1}{2}
        \underbrace{
          \left(
            \frac{1}{\tau^2} + \frac{n}{\sigma^2}
          \right)}_{\displaystyle\phi}
        \left[
          \theta^2 -
          \frac{2 \theta \mu/\tau^2}{(\frac{1}{\tau^2} +
            \frac{n}{\sigma^2})} -
          \frac{-2 \theta \sum_{t=1}^n x_t/\sigma^2}{(\frac{1}{\tau^2} +
            \frac{n}{\sigma^2})} +
          \text{cte}
        \right] \displaybreak[0] \\
        &= - \frac{1}{2} \phi
        \left[
          \theta^2 - 2 \theta
          \left(
            \frac{\mu/\tau^2 + \sum_{t=1}^n x_t/\sigma^2}{\phi}
          \right) +
          \text{cte}
        \right] \displaybreak[0] \\
        &= - \frac{1}{2}
        \frac{
          \left(
            \theta -
            \left(
              \frac{\mu}{\phi \tau^2} +
              \frac{\sum_{t=1}^n x_t}{\phi \sigma^2}
            \right)
          \right)^2}{1/\phi} +
        \text{cte}.
      \end{align*}
      Par conséquent, $\Theta|X_1, \dots, X_n \sim
      N(\tilde{\mu}, \tilde{\tau}^2)$ avec
      \begin{align*}
        \tilde{\tau}^2
        &= \frac{1}{\phi} \\
        &= \frac{\tau^2 \sigma^2}{\sigma^2 + n \tau^2} \\
        \intertext{et}
        \tilde{\mu}
        &= \frac{\mu}{\phi \tau^2} +
        \frac{\sum_{i = 1}^n X_i}{\phi \sigma^2} \\
        &= \frac{\mu \sigma^2 + \tau^2 \sum_{i = 1}^n X_i}{%
          \sigma^2 + n \tau^2}.
      \end{align*}
    \item On sait de l'\autoref{ex:rappels:mediane} que le minimum
      d'une fonction de perte absolue est atteint à la médiane de la
      variable aléatoire. Ainsi, le minimum de
      $\esp{\abs{\hat{\theta} - \theta}|X_1, \dots, X_n}$ est atteint
      à la médiane de $\Theta|X_1, \dots, X_n$. Or, la médiane d'une
      loi normale est égale à sa moyenne, d'où
      $\hat{\theta} = \tilde{\mu}$, où $\tilde{\mu}$ est tel que
      défini en a).
    \end{enumerate}
  \end{sol}
\end{exercice}

\Closesolutionfile{solutions}
\Closesolutionfile{reponses}

\section*{Exercices proposés dans \cite{Wackerly:mathstat:7e:2008}}

\begin{trivlist}
\item \emph{Il n'y a pas de solutions clips pour ces exercices}
\item 16.1, 16.6, 16.7, 16.10, 16.12
\end{trivlist}


%%%
%%% Insérer les réponses
%%%
%\input{reponses-bayesienne}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "analyse-statistique-exercices-et-solutions"
%%% coding: utf-8
%%% End:
