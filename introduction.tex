%%% Copyright (C) 2010-2022 Vincent Goulet, Mathieu Pigeon
%%%
%%% Ce fichier fait partie du projet «Analyse statistique - Exercices
%%% et solutions».
%%% https://gitlab.com/vigou3/analyse-statistique-exercices-et-solutions
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0 International de
%%% Creative Commons. https://creativecommons.org/licenses/by-sa/4.0/

\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}
\markboth{Introduction}{Introduction}

Ce document est une collection des exercices que nous avons utilisés
dans le cadre du cours Analyse statistique des risques actuariels à
l'École d'actuariat de l'Université Laval. Certains exercices sont le
fruit de notre imagination, alors que plusieurs autres sont des
adaptations d'exercices tirés des ouvrages cités dans la
bibliographie.

Le recueil se veut un complément à un cours de statistique
mathématique pour des étudiants de premier ou deuxième cycle
universitaire. Les exercices sont divisés en sept chapitres qui
correspondent aux chapitres de notre cours.

Le \autoref{chap:rappels} porte sur des rappels de notions de base en
probabilité. Il est suivi d'un chapitre qui traite des transformations
de variables aléatoires pour mener à la présentation des lois $t$ de
Student et $F$ de Fisher. L'analyse statistique débute véritablement
au \autoref{chap:echantillon} avec l'étude de la notion d'échantillon
aléatoire, des distributions de la moyenne et de la variance de
l'échantillon, ainsi que des statistiques d'ordre. Le
\autoref{chap:ponctuelle}, qui traite d'estimation ponctuelle par les
méthodes classiques (maximum de vraisemblance, méthode des moments,
etc.) et des diverses propriétés des estimateurs constitue le cœur du
recueil. Le \autoref{chap:bayesienne} propose quelques exercices
d'estimation bayésienne. Enfin, les notions étroitement liées
d'estimation par intervalle et de test d'hypothèses font l'objet des
chapitres~\ref{chap:intervalle} et \ref{chap:tests}.

Nous vous invitons à utiliser le logiciel R \citep{R} pour résoudre
certains exercices, notamment ceux du \autoref{chap:tests}.

Les solutions complètes des exercices se trouvent à
l'\autoref{chap:solutions}. Nous proposons également à la fin de
chaque chapitre des exercices additionnels dans
\citet{Wackerly:mathstat:7e:2008}. Des solutions de ces exercices sont
offertes dans \citet{Wackerly:solutions:7e:2008}, ou encore sous forme
de clips vidéo dans le \link{https://libre.act.ulaval.ca}{Portail
  libre} de l'École d'actuariat.

En consultation électronique, ce document se trouve enrichi de
fonctionnalités interactives:
\begin{itemize}
\item Intraliens entre le numéro d'un exercice et sa solution, et vice
  versa. Ces intraliens sont marqués par la couleur
  \textcolor{link}{\rule{1.5em}{1.2ex}}.
\item Intraliens entre les citations dans le texte et leur entrée dans
  la bibliographie. Ces intraliens sont marqués par la couleur
  \textcolor{citation}{\rule{1.5em}{1.2ex}}.
\item Hyperliens vers des ressources externes marqués par le symbole
  {\smaller\faExternalLink*} et la couleur
  \textcolor{url}{\rule{1.5em}{1.2ex}}.
\item Table des matières et liste des figures permettant d'accéder
  rapidement à des ressources du document.
\end{itemize}

Le projet «\thetitle» s'inscrit dans le mouvement de
l'\link{https://www.gnu.org/philosophy/free-sw.html}{informatique
  libre}. Vous pouvez accéder à l'ensemble du code source en format
{\LaTeX} en suivant le lien dans la page de copyright. Vous trouverez
dans le fichier \code{README.md} toutes les informations utiles pour
composer le document.

Votre contribution à l'amélioration du document est également la
bienvenue; consultez le fichier \code{CONTRIBUTING.md} fourni avec ce
document et voyez votre nom ajouté au fichier \code{COLLABORATEURS}.

Bon travail!

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "analyse-statistique-exercices-et-solutions"
%%% coding: utf-8
%%% End:
